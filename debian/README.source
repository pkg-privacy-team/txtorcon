Debian packaging for txtorcon
=============================

txtorcon is managed in a Git repository using git-buildpackage (with
pristine-tar). It follows most of the advices provided by Russ Allbery at:
http://www.eyrie.org/~eagle/notes/debian/git.html

As upstream is using Git, release tags are merged into the 'upstream' branch of the
Debian package repository when importing new tarballs.

Creating an updated package for a new upstream release goes as:

0. Add a remote with upstream repository if not already done:

    $ git remote add github-upstream https://github.com/meejah/txtorcon.git

1. Fetch new commits from upstream repository:

    $ git fetch github-upstream

2. Verify the tag for the new version:

    $ git tag -v <version>

3. Download and verify the upstream tarball:

    $ uscan

4. Import new upstream tarball (e.g. 0.8):

    $ gbp import-orig ../txtorcon-0.8.tar.gz --upstream-vcs-tag=v0.8 -u0.8 --pristine-tar

5. Hack, improve, update debian/changelog…

6. Build a new package:

    $ gbp buildpackage
